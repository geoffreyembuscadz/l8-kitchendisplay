import Vue from 'vue'
import Router from 'vue-router'

// Area
import areaPage from '../pages/areas.vue';
// Step
import processPage from '../pages/processes.vue';
// To Do
import todoPage from '../pages/to_do.vue';
// Reports Page
import reportPage from '../pages/reports.vue';
// Reports Transaction summary Page
import reportTransactionSummaryPage from '../pages/reports_transaction_summary.vue';
// Item
import itemPage from '../pages/items.vue';
// Import Item
import importItemPage from '../pages/importItems.vue';

import usersPage from '../pages/users.vue'
import homePage from '../pages/home.vue'
import claimPage from '../pages/claim.vue'
import claimingCustomerPage from '../pages/claiming_customer.vue'

Vue.use(Router)

const router = new Router ({
  mode: 'history',
  root: '/home',
  routes:
  [
    {
      path: '/users',
      name: 'users',
      component: usersPage
    },
    {
      path: '/home',
      name: 'home',
      component: homePage
    },
    {
      path: '/claim',
      name: 'claim',
      component: claimPage
    },
    {
      path: '/claiming_customer',
      name: 'claiming_customer',
      component: claimPage
    },
    // Todo
    {
      path: '/to_do',
      name: 'todo',
      component: todoPage
    },
    // Reports
    {
      path: '/reports',
      name: 'reports',
      component: reportPage
    },
    // reportTransactionSummaryPage
    {
      path: '/reports_transaction_summary',
      name: 'reports_transaction_summary',
      component: reportTransactionSummaryPage
    },
    // Items
    {
      path: '/items',
      name: 'item',
      component: itemPage
    },
    // Item Import
    {
      path: '/import_item',
      name: 'import',
      component: importItemPage
    },
    // Areas
    {
      path: '/areas',
      name: 'area',
      component: areaPage
    },
    // Step
    {
      path: '/process',
      name: 'process',
      component: processPage
    },
    {
      path: '/',
      redirect: '/home'
    }
  ]
})

export default router
