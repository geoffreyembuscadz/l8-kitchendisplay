import Vue from 'vue';

export const TransactionTimer = {
  inserted(el) {
  	console.log('transaction-timer inserted', el)
  	// console.log('el: ',el)
  },
  bind(el, bind) {
  	el.style.backgroundColor = 'Red'

  	const estimated_minutes = 15;
  	const datetime_created = (bind.value != null) ? new Date( bind.value ) : new Date();
  	const datetime_estimate = new Date(datetime_created.getTime() + (estimated_minutes * 60000))
  	const current_date_time = new Date()

  	console.log('transaction-timer bind', {
  		current_date_time: current_date_time,
  		datetime_created: datetime_created,
  		datetime_estimate: datetime_estimate
  	})

  	// let timestamp = new Date(that.options.year,
  	// 	that.options.month,
  	// 	that.options.day,
  	// 	that.options.hours,
  	// 	that.options.minutes,
  	// 	that.options.seconds
  	// );

  	// let interval = 1;
  	// let endVal = '';

  	// setInterval(function () {
  	// 	timestamp = new Date(current_date_time.getTime() + interval * 1000);
  	// 	endVal = current_date_time.getHours() + ':' + current_date_time.getMinutes() + ':' + current_date_time.getSeconds();
  	// }, Math.abs(interval) * 1000);

  	if(current_date_time > datetime_estimate){
  		console.log('late')
  	} else {
  		console.log('not late')
  	}
  }
}

// Register a global custom directive called `v-time`
Vue.directive('transaction-timer', TransactionTimer )

