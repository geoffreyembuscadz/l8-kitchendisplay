// import 'material-design-icons-iconfont/dist/material-design-icons.css' 
// import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import router from './router'
import './stylus/main.styl'

Vue.use(Vuetify, {
  theme: {
    primary: '#1867c0', //Main color
    primaryText: '#FFFFFF', //Color for text on primary
    secondary: '#008d4c', //Color for active nav
    lightText: '#000000' //Dark Text for light Backgrounds
  }
})

//Create the App with the router
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
