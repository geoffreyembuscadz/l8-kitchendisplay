//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')

//Define all of its fields, like columns of a SQL table
const definition = {
  trans_details_id: {
    type: String,
    required: true
  },
  trans_id: {
    type: String,
    required: true
  },
  item_id: {
    type: String,
    required: false
  },
  item_name: {
    type: String,
    required: false
  },
  qty: {
    type: String,
    required: true,
    default: 1
  },
  created_at: {
    type: String,
    required: true
  },
  updated_at: {
    type: String,
    required: false,
    default: ""
  },
  step_no: {
    type: Number,
    required: false
  },
  area: {
    type: String,
    required: false,
    default: ""
  },
  start_time: {
    type: String,
    required: false,
    default: ""
  },
  end_time: {
    type: String,
    required: false,
    default: ""
  },

  status: {
    type: Boolean,
    required: false,
    default: false
  }
}

//Set any options for the schema
const options = {
  timestamps: true
}

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const transactionDetailsAuditSchema = new mongoose.Schema(definition)

//export that boye
module.exports = mongoose.model('TransactionDetailsAudit', transactionDetailsAuditSchema)
