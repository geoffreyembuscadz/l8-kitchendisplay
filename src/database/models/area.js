//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')

//Define all of its fields, like columns of a SQL table
const definition = {
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  printer_ip: {
    type: String,
    required: false
  },
  controller: {
    type: Number,
    required: false,
    default: 0
  },
  status: {
    type: Boolean,
    required: false,
    default: true
  }
}

//Set any options for the schema
// const options = {
//   timestamps: true
// }

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const areaSchema = new mongoose.Schema(definition)

//export that boye
module.exports = mongoose.model('Area', areaSchema)
