//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')

//Define all of its fields, like columns of a SQL table
const definition = {
  name: {
    type: String,
    required: true
  },
  item_id: {
    type: Number,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  preparation_time_mins: {
    type: Number,
    required: false
  },
  process: {
    type: Object,
    required: true
  },
  status: {
    type: String,
    required: false,
    default: "Active"
  }
}

//Set any options for the schema #mindBLOWJOB
const options = {
  timestamps: true
}

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const itemSchema = new mongoose.Schema(definition, options)

//export that boye
module.exports = mongoose.model('Item', itemSchema)
