//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')

//Define all of its fields, like columns of a SQL table
const definition = {
  task_title: {
    type: String,
    required: true
  },
  task_description: {
    type: String,
    default: '',
    required: false
  },
  interval_mins: {
    type: Number,
    required: true,
    default: 0
  },
  reminder_datetime: {
    type: String,
    required: false,
    default: ''
  },
  status: {
    type: Boolean,
    required: false,
    default: true
  }
}

//Set any options for the schema
const options = {
  timestamps: true
}

//make the schema as a new instance of a mongoose schema, using our definition and options
const todoSchema = new mongoose.Schema(definition, options)

//export that boye
module.exports = mongoose.model('Todo', todoSchema)
