//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')

//Define all of its fields, like columns of a SQL table
const definition = {
  trans_id: {
    type: String,
    default: "",
    required: true
  },
  item_id: {
    type: Number,
    default: "",
    required: false
  },
  step_no: {
    type: Number,
    required: true
  },
  qty: {
    type: Number,
    required: true,
    default: 1
  },
  start_time: {
    type: String,
    required: false,
    default: ""
  },
  end_time: {
    type: String,
    required: false,
    default: ""
  }
}

//Set any options for the schema
const options = {
  timestamps: true
}

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const transactionDetailsTimerSchema = new mongoose.Schema(definition)

//export that boye
module.exports = mongoose.model('TransactionDetailsTimer', transactionDetailsTimerSchema)
