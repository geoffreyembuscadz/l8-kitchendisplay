//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Define all of its fields, like columns of a SQL table
const definition = {
  trans_id: {
    type: String,
    required: true
  },
  customer_name: {
    type: String,
    required: false
  },
  table_name: {
    type: String,
    required: false
  },
  service_type: {
    type: String,
    required: false
  },
  priority_level: {
    type: Number,
    required: false
  },
  discount_code: {
    type: String,
    required: false,
    default: ""
  },
  created_at: {
    type: String,
    required: false
  },
  completed_at: {
    type: String,
    required: false
  },
  status: {
    type: Number,
    required: true,
    default: 0
  }
  // ,items: [{ type: Schema.Types.ObjectId, ref: 'TransactionDescription' }]
}

//Set any options for the schema
const options = {
  timestamps: true
}

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const transSchema = new mongoose.Schema(definition, options)

//export that boye
module.exports = mongoose.model('Transaction', transSchema)
