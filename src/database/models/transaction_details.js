//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Define all of its fields, like columns of a SQL table
const definition = {
  trans_id: {
    type: String,
    required: true
  },
  id: {
    type: Number,
    required: false
  },
  name: {
    type: String,
    required: false
  },
  item_code: {
    type: String,
    required: false,
    default: ""
  },
  uom: {
    type: String,
    required: false
  },
  qty: {
    type: Number,
    required: true
  },
  serving_time: {
    type: Number,
    required: true
  },
  current_step: {
    type: Number,
    required: true,
    default: 1
  },
  note: {
    type: String,
    required: false
  },
  running_status: {
    type: Number,
    required: false,
    default: 0
  },
  status: {
    type: Number,
    required: true,
    default: 0
  }
  // ,transaction: { type: Schema.Types.ObjectId, ref: 'Transaction' }
}

//Set any options for the schema
const options = {
  timestamps: true
}

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const transactionDetailsSchema = new mongoose.Schema(definition, options)

//export that boye
module.exports = mongoose.model('TransactionDetails', transactionDetailsSchema)
