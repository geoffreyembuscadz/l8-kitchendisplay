//import mongoose, our ODM for mongoDB
const mongoose = require('mongoose')

//Define all of its fields, like columns of a SQL table
const definition = {
  node_env: {
    type: String,
    required: true,
    default: 'development'
  },
  mongo_uri: {
    type: String,
    required: false,
    default: 'mongodb://127.0.0.1:27017/logic8_kds'
  },
  // Socket Port
  socket_port: {
    type: String,
    required: false,
    default: '8000'
  },
  
  // API Configs
  api_host: {
    type: String,
    required: false,
    default: '127.0.0.1'
  },
  api_port: {
    type: String,
    required: false,
    default: '8080'
  },

  // Web URL
  web_host: {
    type: String,
    required: false,
    default: '127.0.0.1'
  },
  web_port: {
    type: String,
    required: false
  },
  production_url: {
    type: String,
    required: false
  },
}

//Set any options for the schema
// const options = {
//   timestamps: true
// }

//make the schema as a new instance of a mongoose schema, using our definition and options
// const userSchema = new mongoose.Schema(definition)
const configurationSchema = new mongoose.Schema(definition)

//export that boye
module.exports = mongoose.model('Configuration', configurationSchema)
