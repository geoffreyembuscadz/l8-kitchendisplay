//bring in the seperate models
const User = require('./user')
//import other models in the same manner
const Area = require('./area')
const Item = require('./item')
const Configuration = require('./configuration')
const Process = require('./process')
const Todo = require('./to_do')
const Transactions = require('./transaction')
const TransactionDetails = require('./transaction_details')
const TransactionDetailsAudit = require('./transaction_details_audit')
const TransactionDetailsTimer = require('./transaction_details_timer')

//export em in a good ol' bundle
module.exports = {
  User,
  Area,
  Item,
  Todo,
  Process,
  Transactions,
  Configuration,
  TransactionDetails,
  TransactionDetailsTimer,
  TransactionDetailsAudit
}
