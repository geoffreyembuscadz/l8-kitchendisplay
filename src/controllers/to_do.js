//import the todo constant explicitly
const { Todo } = require('../database/models')

//show all todos
exports.index = async (req, res) => {

  //query the DB of all todos
  const todos = await Todo.find().exec()
  // const todos = [{id: 1, name: 'Bar'}, {id: 2, name: 'Kitchen'}];

  //send the response
  res.json({ data: todos })

  return
}

//Store a new todo
exports.store = async (req, res) => {
  //create a new todo object with the request body
  const todo = new Todo(req.body)

  //save it in the DB
  await todo.save()

  //send a 201 and the new resource
  res.status(201).json({ data: todo })
}

//this function is for looking at one todo by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const todo = await Todo.findById(req.params.id).exec()

  //send him back home
  res.json({ data: todo })
}

//ever wanted to make the todos disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await Todo.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a todo based on ID
exports.update = async (req, res) => {
  const todo = await Todo
  .findByIdAndUpdate(req.params.id, req.body, { new: true })
  .exec()
res.json({ data: todo })
}
