//import the Step constant explicitly
const { TransactionDetails, Transactions, TransactionDetailsAudit } = require('../database/models')

//show all steps
exports.index = async (req, res) => {

  //query the DB of all steps
  const TransactionDetailses = await TransactionDetails.aggregate([{
    $lookup: {
      from: 'items',
      localField: 'id',
      foreignField: 'item_id',
      as: 'item'
    }
  }]).exec()

  //send the response
  res.json({ data: TransactionDetailses })

  return
}

//Store a new Step
exports.store = async (req, res) => {

  //create a new area object with the request body
  const TransactionDetailses = new TransactionDetails(req.body)

  //save it in the DB
  await TransactionDetailses.save()

  //send a 201 and the new resource
  res.status(201).json({ data: req.body })
}

//this function is for looking at one Step by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const TransactionDetailses = await TransactionDetails.findById(req.params.id).exec()

  //send him back home
  res.json({ data: TransactionDetailses })
}

//ever wanted to make the steps disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await TransactionDetails.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a Step based on ID
exports.update = async (req, res) => {
  let result_itms = null;
  let raw_req_data = req.body;

  const TransactionDetailses = await TransactionDetails
    .findByIdAndUpdate(req.params.id, req.body, { new: true })
    .exec((err, result_items) => {
      result_itms = result_items
      /*
      console.log('result_itms', result_itms);
      console.log('raw_req_data', raw_req_data);
      console.log(raw_req_data.item_info);
      */

      let item_info = raw_req_data.item_info;

      if(item_info.length > 0){
        TransactionDetailsAudit.count({ trans_details_id:result_itms._id }, (err, count) => {
          console.log('TransactionDetailsAudit', count);
          let item_steps = raw_req_data.item_info[0].process.steps;
          let current_datetime = new Date();
          current_datetime = current_datetime.toLocaleString(undefined, {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
          });

          if(result_itms.status == true){
            TransactionDetails.findByIdAndUpdate( result_itms._id, { updatedAt: current_datetime, running_status: 0 }, (err, raw) => { });
          } else {
            TransactionDetails.findByIdAndUpdate( result_itms._id, { updatedAt: current_datetime }, (err, raw) => { });
          }
          // running timer
        });
      }
    })

  res.json({ data: TransactionDetailses })
}
