//import the todo constant explicitly
const { TransactionDetailsAudit } = require('../database/models')

//show all todos
exports.index = async (req, res) => {
  const trans_id = req.query.trans_id;
  const query = {};

  if(trans_id !== undefined){
    query.trans_id = trans_id
  }

  //query the DB of all todos
  const transaction_details_audit = await TransactionDetailsAudit.find(query).exec()
  // const todos = [{id: 1, name: 'Bar'}, {id: 2, name: 'Kitchen'}];

  //send the response
  res.json({ data: transaction_details_audit })

  return
}

//Store a new todo
exports.store = async (req, res) => {
  //create a new todo object with the request body
  const transaction_details_audit = new TransactionDetailsAudit(req.body)

  //save it in the DB
  await transaction_details_audit.save()

  //send a 201 and the new resource
  res.status(201).json({ data: transaction_details_audit })
}

//this function is for looking at one todo by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const transaction_details_audit = await TransactionDetailsAudit.findById(req.params.id).exec()

  //send him back home
  res.json({ data: transaction_details_audit })
}

//ever wanted to make the todos disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await TransactionDetailsAudit.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a todo based on ID
exports.update = async (req, res) => {
  const transaction_details_audit = await TransactionDetailsAudit
  .findByIdAndUpdate(req.params.id, req.body, { new: true })
  .exec()
res.json({ data: transaction_details_audit })
}
