//import the Step constant explicitly
const { Process, Item } = require('../database/models')

//show all steps
exports.index = async (req, res) => {

  //query the DB of all steps
  const processes = await Process.find().exec()

  //send the response
  res.json({ data: processes })

  return
}

//Store a new Step
exports.store = async (req, res) => {

  //create a new area object with the request body
  const processes = new Process(req.body)

  //save it in the DB
  await processes.save()

  //send a 201 and the new resource
  res.status(201).json({ data: req.body })
}

//this function is for looking at one Step by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const processes = await Process.findById(req.params.id).exec()

  //send him back home
  res.json({ data: processes })
}

//ever wanted to make the steps disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await Process.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a Step based on ID
exports.update = async (req, res) => {
  // let mongoose = require('mongoose');

  const processes = await Process.findByIdAndUpdate(req.params.id, req.body, { new: true })
    .exec((err, result_process) => {
      let process_data = result_process;
      let steps_updated = [];
      // Item
      Object.keys(process_data.steps).forEach((step_key) => {
        steps_updated.push(process_data.steps[step_key]);
      });

      process_data.steps = [];
      process_data.steps = steps_updated;

      Item.update({ 'process._id' : result_process.id }, { 'process':process_data }, { multi: true },  (error) => {
        console.log('item updated');
      })
    })

  res.json({ data: processes })
}
