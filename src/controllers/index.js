//Here we will import all our controllers, this way we only need to
//import the controller as a single module, or an explicit constant later!!!
const usersController = require('./users')
const areasController = require('./areas')
const itemsController = require('./items')
const processesController = require('./processes')
const importItemsController = require('./import_items')
const toDoController = require('./to_do')
const reportsController = require('./reports')
const transactionsController = require('./transactions')
const configurationController = require('./configurations')
const transactionDetailsController = require('./transaction_details')
const transactionDetailsAuditController = require('./transaction_details_audit')
const transactionDetailsTimerController = require('./transaction_details_timer')

module.exports = {
  areasController,
  processesController,
  toDoController,
  itemsController,
  reportsController,
  importItemsController,
  transactionsController,
  configurationController,
  transactionDetailsController,
  transactionDetailsAuditController,
  transactionDetailsTimerController,
  usersController
}
