const { Item } = require('../database/models')

const mysql = require('mysql')
const fsCsv = require('fast-csv')
// controllers .... if 0 is POS
exports.getItemsFromDB = function(req, res) {

	let req_data = req.body;
	let itemsData = [];
	
	let mysql_connection = mysql.createPool({
		connectionLimit : 200,
		host: req_data.host,
		user: req_data.username,
		password : req_data.password,
		database : req_data.database_name
		// ,connectTimeout: 60000,
		// connectTimeout: 60000,
		// acquireTimeout: 60000
	});

	/*
	SELECT
	i.id AS item_id, i.name, "" AS description, "" AS step_id, 1 AS status, "" AS process_check
	from items i
	JOIN item_categories c ON c.id = i.category 
	JOIN departments d ON d.id = i.department_id 
	where i.is_active = 1 and i.is_menu = 1 and i.is_warehouse = 0;
	*/

	let selectQuery = 'SELECT [columns] FROM [table_name] [joins] WHERE i.is_active = 1 and i.is_menu = 1 and i.is_warehouse = 0 ORDER BY TRIM(i.name) ;';
	let selectQueryColumn = 'DISTINCT i.id AS item_id, TRIM(i.name) AS name, "" AS description, "" AS step_id, 1 AS status, "" AS process_check, "12" AS preparation_time_mins';
	let selectQueryTable = 'items i';
	let selectQueryJoins = 'JOIN item_categories c ON c.id = i.category JOIN departments d ON d.id = i.department_id ';
	let selectQueryItemId = '';

	req_data.table_name = (req_data.table_name == "") ? null : req_data.table_name;
	req_data.item_id = (req_data.item_id == "") ? null : req_data.item_id;

	if(req_data.table_name != null){
		selectQueryTable = req_data.table_name + ' i';
	}

	if(req_data.item_id != null){
		selectQueryItemId = ' AND IN(' + req_data.item_id + ')';
	}

	selectQuery = selectQuery.replace("[joins]", selectQueryJoins);
	selectQuery = selectQuery.replace("[columns]", selectQueryColumn);
	selectQuery = selectQuery.replace("[item_id]", selectQueryItemId);
	selectQuery = selectQuery.replace("[table_name]", selectQueryTable);
	console.log('req_data_mysql', req_data);
	console.log('query', selectQuery);

	mysql_connection.query(selectQuery, function (error, results, fields) {
		if (error){ console.log(results); console.log(error); throw error; }
		
		res.send({ data: results });
		this.end();
	});
}

exports.download = function (req, res) {
	res.send('File uploaded!');
}

//Store a new area by upload
exports.upload = function (req, res) {
	console.log('post multer', req.file); // undefined
	if(!req.file){
		return res.status(400).send('No files were uploaded.');
	} else {
		// console.log('file data', req.file.path ); // {}
	}

	let csvFileDatas = [];
	let fileItems = req.file.filename;
	let fullFilePath = __dirname + '/../../' + req.file.path;

	// CSV File Data Reader
	fsCsv
	 .fromPath(fullFilePath)
	 .on("data", function(data){
	 	console.log('data-row-upload', data);

	 	if(data[0] != '' && data[1] != ''){
	 		let item_id = (data[1] != null || data[1] != "") ? data[1] : "";
		 	// DISTINCT i.id AS item_id, NULLIF(code,"N/A") as code, TRIM(i.name) AS name, "" AS description, "" AS step_id, 1 AS status, "" AS process_check, "" AS preparation_time_mins
		 	let row_item_data = { item_id: item_id, name: data[0], description: "", step_id: "" , status: 1, process_check: "", preparation_time_mins: 12 };
		    console.log('raw data: ', csvFileDatas);
		    console.log('tanda_tanda: ', row_item_data);

		    if(data[0] != 'Item Name' && data[1] != 'Item ID'){
		    	csvFileDatas.push(row_item_data);
		    }
	 	}
	 })
	 .on("end", function(){
	 	console.log("done");
	 	console.log(csvFileDatas);
	 	return res.status(201).json({ data: csvFileDatas })
	 	// csvFileDatas		
	 });
	 
	stream.pipe(csvStream);
	res.send('File uploaded!');
}

//Store a new area from upload
exports.submitImportedItems = function (req, res) {
	let importedItems = req.body;
	let itemBulk = Item.collection.initializeUnorderedBulkOp();

	importedItems.forEach((record) => {
		let query = { item_id: Number(record.item_id) };

		itemBulk.find(query).upsert().updateOne( record );
	});

	itemBulk.execute((err, bulkItemRes) => {
		res.status(201).json({ data: bulkItemRes, data_length: bulkItemRes.length });
	});

	// Old Code, Bulk Insert Only
	// 	let importedItems = req.body;

	// 	Item.collection.insert(importedItems, (err, docs) => {
	// 		return res.status(201).json({ data: docs, data_length: docs.length });
	// 	});
}
