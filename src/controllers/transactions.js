//import the area constant explicitly
const mongoose = require('mongoose')
const { Transactions, TransactionDetails, TransactionDetailsTimer, Item } = require('../database/models')

//show all areas
exports.index = async (req, res) => {
  const area_id = req.query.area
  const trans_id = req.query.trans_id
  let match = {
    status: 0 // Status is filtered to 0 by default, 0 = Transactions that haven't labelled as "Out"
  };

  /* if(req.query.trans_id){
    match = { status: 0, trans_id: trans_id };
  } */
  if(req.query.status){
    match.status = Number(req.query.status);
  }

  Transactions.aggregate([{
        $lookup: {
          from: 'transactiondetails',
          localField: 'trans_id',
          foreignField: 'trans_id',
          as: 'transdetails'
        }
      },
      {
        $unwind: {
          path: '$transdetails',
          preserveNullAndEmptyArrays: true
        }
      }, {
        $lookup: {
          from: 'items',
          localField: 'transdetails.id',
          foreignField: 'item_id',
          as: 'transdetails.item_info'
        }
      }, {
        $group: {
          _id: '$_id',
          trans_id: {
            $first: '$trans_id'
          },
          table_name: {
            $first: '$table_name'
          },
          service_type: {
            $first: '$service_type'
          },
          customer_name: {
            $first: '$customer_name'
          },
          priority_level: {
            $first: '$priority_level'
          },
          discount_code: {
            $first: '$discount_code'
          },
          status: {
            $first: '$status'
          },
          created_at: {
            $first: '$created_at'
          },
          items: {
            $push: '$transdetails'
          }
        }
      },{
        $match: match
      }]).sort({ priority_level: 'desc', trans_id: 'asc' }).exec((err, result_transactions) => {
        res.json({ data: result_transactions })
          return
      });

}

exports.getTransForClaim = async(req, res) => {
  const status = req.query.status;

  const transactions = await Transactions.find().
                    where('status').equals(status).
                    sort({ priority_level: 'desc', trans_id: 'asc' }).
                    exec();

  res.json({ data: transactions })
}



//Store a new area
exports.store = async (req, res) => {

  //create a new area object with the request body
  const transactions = new Transactions(req.body)

  //save it in the DB
  await transactions.save()

  //send a 201 and the new resource
  res.status(201).json({ data: transactions })
}

//this function is for looking at one area by their mongo id
exports.show = async (req, res) => {
  let transaction_id = req.params.id;
  //find this sneaky boye // findById(req.params.id).
  // const transactions = await Transactions.findById(req.params.id).exec()
  const transactions = await Transactions.
    aggregate([{
        $lookup: {
          from: 'transactiondetails',
          localField: 'trans_id',
          foreignField: 'trans_id',
          as: 'transdetails'
        }
      },{
        $unwind: {
          path: '$transdetails',
          preserveNullAndEmptyArrays: true
        }
      }, {
        $lookup: {
          from: 'items',
          localField: 'transdetails.id',
          foreignField: 'item_id',
          as: 'transdetails.item_info'
        }
      }, {
        $group: {
          _id: '$_id',
          trans_id: {
            $first: '$trans_id'
          },
          table_name: {
            $first: '$table_name'
          },
          service_type: {
            $first: '$service_type'
          },
          customer_name: {
            $first: '$customer_name'
          },
          priority_level: {
            $first: '$priority_level'
          },
          discount_code: {
            $first: '$discount_code'
          },
          status: {
            $first: '$status'
          },
          items: {
            $push: '$transdetails'
          }
        }
      },{
        $match: {
          _id: mongoose.Types.ObjectId(transaction_id)
          // Status is filtered to 0 by default, 0 = Transactions that haven't labelled as "Out"
        }
      }]).exec((err, result_transactions) => {
        // console.log('if item_ids > 0 return_transactions', { data: result_transactions });
      
        res.json({ data: result_transactions })
        return
      });
}

exports.getTransForClaim = async(req, res) => {
  const status = req.query.status;

  console.log('status: ', status);

  const transactions = await Transactions.find().
                    where('status').equals(status).
                    sort({ priority_level: 'desc', trans_id: 'asc' }).
                    exec();

  res.json({ data: transactions })
}

//Store a new area
exports.store = async (req, res) => {

  //create a new area object with the request body
  const transactions = new Transactions(req.body)

  //save it in the DB
  await transactions.save()

  //send a 201 and the new resource
  res.status(201).json({ data: transactions })
}

//ever wanted to make the areas disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await Transactions.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a area based on ID
exports.update = async (req, res) => {
  let match = { status: 0 };
  let transaction_data = null;
  let request_datas = req.body;
  let current_datetime = new Date();
  let transaction_details_req = req.body.items;

  if(request_datas.status > 1){
    const transactions = await Transactions
    .findByIdAndUpdate(req.params.id, req.body, { new: true })
    .exec((err, result_transaction) => {
      transaction_data = result_transaction
      console.log('update_transaction_data', transaction_data)
    })

    // res.json({ data: transactions })
  } else {
    const transactions = await Transactions
    .findByIdAndUpdate(req.params.id, req.body, { new: true })
    .exec((err, result_transaction) => {
      transaction_data = result_transaction
      console.log('update_transaction_data', transaction_data)
    })

    TransactionDetailsTimer.update({ trans_id: request_datas.trans_id, end_time: '' }, { end_time: current_datetime.toLocaleString() }, { multi: true }, (err, raw) => { });

    TransactionDetails.find({ trans_id: transaction_data.trans_id })
      .select('status')
      .exec((err, result_items) => {
        let completed = 0;
        let not_completed = 0;
        let transaction_details = result_items;

        Object.keys(transaction_details).forEach((item_key) => {
          if(transaction_details[item_key].status == 1){
            completed++;
          } else if(transaction_details[item_key].status == 0){
            not_completed++;
          }
        })

        if(not_completed == 0 && completed > 0){
          Transactions.update({ trans_id: transaction_data.trans_id },
            { status: 1 }, (err, numberAffected, rawResponse) => { });
        } else {
          Transactions.update({ trans_id: transaction_data.trans_id },
            { status: 0 }, (err, numberAffected, rawResponse) => { });
        }
      });
    // res.json({ data: transactions })
  }

  // Transactions Index //
  Transactions.aggregate([{
        $lookup: {
          from: 'transactiondetails',
          localField: 'trans_id',
          foreignField: 'trans_id',
          as: 'transdetails'
        }
      },
      {
        $unwind: {
          path: '$transdetails',
          preserveNullAndEmptyArrays: true
        }
      }, {
        $lookup: {
          from: 'items',
          localField: 'transdetails.id',
          foreignField: 'item_id',
          as: 'transdetails.item_info'
        }
      }, {
        $group: {
          _id: '$_id',
          trans_id: {
            $first: '$trans_id'
          },
          table_name: {
            $first: '$table_name'
          },
          service_type: {
            $first: '$service_type'
          },
          customer_name: {
            $first: '$customer_name'
          },
          priority_level: {
            $first: '$priority_level'
          },
          discount_code: {
            $first: '$discount_code'
          },
          status: {
            $first: '$status'
          },
          created_at: {
            $first: '$created_at'
          },
          items: {
            $push: '$transdetails'
          }
        }
      },{
        $match: match
      }]).sort({ priority_level: 'desc', trans_id: 'asc' }).exec((err, result_transactions) => {
        res.json({ data: result_transactions })
          return
      });
}

exports.clearClaims = async (req, res) => {
  /*
  status = 1 - Ready for Claim, status = 2 - Claimed
  */
  let query = { status: 1 };
  let update = { status: 2 };
  let from_module = req.body.from_module;

  if(from_module == 'home'){
    query = { status: 0 };
    console.log('home')

    TransactionDetails.update(query, { status: 1 }, { new:true, multi: true },  (err, result_transactiond) => {
      console.log('transactiondetails update clear');
    })
  }

  const transactions = await Transactions
    .update(query, update, { new: true, multi: true }, (err, result_transaction) => {
      transaction_data = result_transaction
      console.log('update_transaction_data', transaction_data)
    })

  res.json({ data: req.body })
}

exports.undoStatuses = async(req, res) => {
    /* This function is for Development purposes only. */

    TransactionDetails.update({}, { status: 0, current_step: 1 }, { new:true, multi: true },  (err, result_transactiond) => {
      console.log('update trans details secret function execute');
    })

    const transactions = await Transactions
    .update({}, { status: 0 }, { new: true, multi: true }, (err, result_transaction) => {
      console.log('secret undo status trans executed')
    })

    res.json({ data: req.body })
}
