//import the todo constant explicitly
const { Transactions, TransactionDetails, Item } = require('../database/models')

// Get most items
exports.getTransactionDetailsDuration = async(req, res) => {
  // const status = req.query.status
  /*
    aggregate project
    {
      trans_id: 1,
      item_name: '$name',
      quantity: '$qty',
      time_different_min: {
      $divide: [{
        $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
      }, 60000 ]
    }
    }
  */
  const transaction_id = req.params.transaction_id;

  TransactionDetails.aggregate([{
    $project: {
      trans_id: 1,
      item_name: '$name',
      quantity: '$qty',
      createdAt: 1,
      updatedAt: 1,
      time_difference_hour: {
        $divide: [{
          $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
        },
        3600000]
      },
      time_different_min: {
        $divide: [{
          $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
        }, 60000 ]
      },
      time_different_sec: {
        $divide: [{
          $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
        }, 1000 ]
      }
    }
  },{
    $match: {
      "trans_id" : transaction_id
    }
  }]).exec((err, most_qty_items) => {
    res.json({ data: most_qty_items })
    return
  });
}

exports.getTransactionsDuration = async(req, res) => {
  Transactions.aggregate([{
        $lookup: {
          from: 'transactiondetails',
          localField: 'trans_id',
          foreignField: 'trans_id',
          as: 'transdetails'
        }
      },{
        $unwind: {
          path: '$transdetails',
          preserveNullAndEmptyArrays: true
        }
      }, {
        $lookup: {
          from: 'items',
          localField: 'transdetails.id',
          foreignField: 'item_id',
          as: 'transdetails.item_info'
        }
      }, {
        $group: {
          _id: '$_id',
          trans_id: {
            $first: '$trans_id'
          },
          table_name: {
            $first: '$table_name'
          },
          service_type: {
            $first: '$service_type'
          },
          customer_name: {
            $first: '$customer_name'
          },
          priority_level: {
            $first: '$priority_level'
          },
          createdAt: { $first: '$createdAt' },
          updatedAt: { $first: '$updatedAt' },
          trans_details: {
            $push: '$transdetails'
          }
        }
      }, {
        $project: {
          _id: '$_id',
          trans_id: 1,
          time_difference_hour: {
            $divide: [{
              $subtract: [
                "$updatedAt",
                "$createdAt"
              ]
            },
            3600000]
          },
          time_different_min: {
            $divide: [{
              $subtract: [
                "$updatedAt",
                "$createdAt"
              ]
            }, 60000 ]
          },
          time_different_sec: {
            $divide: [{
              $subtract: [
                "$updatedAt",
                "$createdAt"
              ]
            }, 1000 ]
          },
          table_name: 1,
          service_type: 1,
          customer_name: 1,
          priority_level: 1,
          trans_details: 1,
          total_quantity: { $sum: "$trans_details.qty"}
        }
      }]).sort({trans_id: 'desc'}).exec((err, trans_records) => {
    res.json({ data: trans_records })
    return
  });
}

exports.exportTransactionDetailsDuration = async(req, res) => {
  const transaction_id = req.params.transaction_id;

  TransactionDetails.aggregate([{
    $project: {
      trans_id: 1,
      item_name: '$name',
      quantity: '$qty',
      createdAt: 1,
      updatedAt: 1,
      time_difference_hour: {
        $divide: [{
          $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
        },
        3600000]
      },
      time_different_min: {
        $divide: [{
          $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
        }, 60000 ]
      },
      time_different_sec: {
        $divide: [{
          $subtract: [
          "$updatedAt",
          "$createdAt"
          ]
        }, 1000 ]
      }
    }
  },{
    $match: {
      "trans_id" : transaction_id
    }
  }]).exec((err, most_qty_items) => {
    /*
      Packages No Longer Need! - export-to-csv,fast-csv
    */
    const json2csv = require('json2csv').parse;
    const fields = [ 'item_name', 'quantity', 'time_difference_hour', 'time_different_min', 'time_different_sec' ];
    const opts = { fields };
    const csv = json2csv(most_qty_items, opts);

    // CSV File Naming
    let date_instance = new Date();
    let csv_file_name = 'kds_transaction_details_duration_' + date_instance.getMonth() + date_instance.getDate() + date_instance.getFullYear();

    res.writeHead(200, {
      'Content-Type': 'text/csv',
      'Content-Disposition': 'attachment; filename=' + csv_file_name + '.csv'
    });
    res.end(csv);
  });
}
