const mongoose = require('mongoose')
//import the area constant explicitly
const { Area, Item, Process } = require('../database/models')

//show all areas
exports.index = async (req, res) => {

  //query the DB of all areas
  const areas = await Area.find().exec()
  // const areas = [{id: 1, name: 'Bar'}, {id: 2, name: 'Kitchen'}];

  //send the response
  res.json({ data: areas })

  return
}

// get all areas as string array
exports.getAreas = async(req, res) => {
  const areas = await Area.find({}, 'name')
                .exec()

  res.json({data: areas})

  return
}

//Store a new area
exports.store = async (req, res) => {
  //create a new area object with the request body
  const area = new Area(req.body)

  //save it in the DB
  await area.save()

  //send a 201 and the new resource
  res.status(201).json({ data: area })
}

//this function is for looking at one area by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const area = await Area.findById(req.params.id).exec()

  //send him back home
  res.json({ data: area })
}

//ever wanted to make the areas disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await Area.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a area based on ID
exports.update = async (req, res) => {
  const area = await Area
  .findByIdAndUpdate(req.params.id, req.body, { new: true })
  .exec((err, result_process) => {
    console.log('result_process._id',result_process._id);
    console.log('result_process.name',result_process.name);
    
    let updated_area_id = String(result_process._id);
    let updated_area_name = String(result_process.name);

    // Update All Areas tagges in Processes (DO NOT TOUCH THIS CODE!)
    Process.find({ 'steps.area_id' : updated_area_id })
    .exec((err, result_items) => {
      let result_item_processes = result_items;

      // PROCESS
      Object.keys(result_item_processes).forEach((process_key) => {
        let process_data = result_item_processes[process_key];
        let process_steps = process_data.steps;

        // STEPS - Areas
        Object.keys(process_steps).forEach((process_steps_key) => {
          let process_steps_areas = process_steps[process_steps_key];
          let new_area_data = { area_name: '' };

          if(process_steps_areas.area_id == updated_area_id){
            new_area_data = process_steps_areas;
            new_area_data.area_name = updated_area_name;

            result_item_processes[process_key].steps[process_steps_key] = new_area_data;
          }
        })

        Process.update({ _id: process_data._id }, { steps: result_item_processes[process_key].steps }, (err, numberAffected, rawResponse) => { });

        console.log('processid', process_data.id);

        Item.update({ 'process._id': mongoose.Types.ObjectId(process_data._id)  }, { process: result_item_processes[process_key] }, (err, numberAffected, rawResponse) => { });

      });
    });
  });
  
  res.json({ data: area })
}
