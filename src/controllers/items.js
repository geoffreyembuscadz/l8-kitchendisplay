//import the area constant explicitly
const { Item } = require('../database/models')

//show all areas
exports.index = async (req, res) => {
  const item_id = req.query.item_id
  const query = {}

  if(item_id !== undefined){
    query.item_id = item_id
  }

  console.log('query', query)

  //query the DB of all areas
  const items = await Item.find(query).exec()

  //send the response
  res.json({ data: items })

  return
}

//Store a new area
exports.store = async (req, res) => {

  //create a new area object with the request body
  const item = new Item(req.body)

  //save it in the DB
  await item.save()

  //send a 201 and the new resource
  res.status(201).json({ data: item })
}

//this function is for looking at one area by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const item = await Item.findById(req.params.id).exec()

  //send him back home
  res.json({ data: item })
}

//ever wanted to make the areas disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await Item.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a area based on ID
exports.update = async (req, res) => {
  const item = await Item
  .findByIdAndUpdate(req.params.id, req.body, { new: true })
  .exec()

  res.json({ data: item })

}
