//import the todo constant explicitly
const { TransactionDetailsTimer, TransactionDetails } = require('../database/models')

//show all todos
exports.index = async (req, res) => {
  const trans_id = req.query.trans_id;
  const trans_details_id = req.query.trans_details_id;
  const current_step = req.query.current_step;
  const allow_empty_end_time = req.query.allow_empty_end_time;
  const query = {};

  if(trans_id !== undefined){
    query.trans_id = trans_id
  }

  if(trans_details_id !== undefined){
    query.trans_details_id = trans_details_id
  }

  if(current_step !== undefined){
    query.current_step = current_step
  }
  console.log(query);

  if(allow_empty_end_time === undefined){
    query.end_time = "";
  }

  //query the DB of all todos
  // const transaction_details_timer = await TransactionDetailsTimer.find().exec()
  // const todos = [{id: 1, name: 'Bar'}, {id: 2, name: 'Kitchen'}];
  TransactionDetailsTimer.aggregate([
    {
      $lookup: {
        from: 'items',
        localField: 'item_id',
        foreignField: 'item_id',
        as: 'item_info'
      }
    },
    { $match : query }
  ]).exec((err, result_transactions) => {
    res.json({ data: result_transactions })
    return
  });

  //send the response
  // res.json({ data: transaction_details_timer })

  // return
}

//Store a new todo
exports.store = async (req, res) => {
  //create a new todo object with the request body
  // let store_trans_details_timer = req.body;

  // const transaction_details_timer = new TransactionDetailsTimer(req.body)

  let update_trans_details_data = {
    'running_status' : 1,
    'updatedAt': datetime_created_at
  };

  //create a new todo object with the request body
  const transaction_details_timer = new TransactionDetailsTimer(req.body)

  //save it in the DB
  await transaction_details_timer.save()

  //send a 201 and the new resource
  res.status(201).json({ data: transaction_details_timer })

  // TransactionDetails.update({ trans_id: store_trans_details_timer.trans_id , items_id: store_trans_details_timer.item_id  }, update_trans_details_data);

  //save it in the DB
  // await transaction_details_timer.save()

  //send a 201 and the new resource
  // res.status(201).json({ data: transaction_details_timer })


/*
  let match = {
    status: 0 // Status is filtered to 0 by default, 0 = Transactions that haven't labelled as "Out"
  };

  Transactions.aggregate([{
        $lookup: {
          from: 'transactiondetails',
          localField: 'trans_id',
          foreignField: 'trans_id',
          as: 'transdetails'
        }
      },
      {
        $unwind: {
          path: '$transdetails',
          preserveNullAndEmptyArrays: true
        }
      }, {
        $lookup: {
          from: 'items',
          localField: 'transdetails.id',
          foreignField: 'item_id',
          as: 'transdetails.item_info'
        }
      }, {
        $group: {
          _id: '$_id',
          trans_id: {
            $first: '$trans_id'
          },
          table_name: {
            $first: '$table_name'
          },
          service_type: {
            $first: '$service_type'
          },
          customer_name: {
            $first: '$customer_name'
          },
          priority_level: {
            $first: '$priority_level'
          },
          discount_code: {
            $first: '$discount_code'
          },
          status: {
            $first: '$status'
          },
          created_at: {
            $first: '$created_at'
          },
          items: {
            $push: '$transdetails'
          }
        }
      },{
        $match: match
      }]).sort({ priority_level: 'desc', trans_id: 'asc' }).exec((err, result_transactions) => {
        res.json({ data: result_transactions })
          return
      });
  */
}

//this function is for looking at one todo by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const transaction_details_timer = await TransactionDetailsTimer.findById(req.params.id).exec()

  //send him back home
  res.json({ data: transaction_details_timer })
}

//ever wanted to make the todos disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await TransactionDetailsTimer.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a todo based on ID
exports.update = async (req, res) => {
  const transaction_details_timer = await TransactionDetailsTimer
  .findByIdAndUpdate(req.params.id, req.body, { new: true })
  .exec()
res.json({ data: transaction_details_timer })
}
