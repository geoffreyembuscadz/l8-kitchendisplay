//import the area constant explicitly
const { Configuration } = require('../database/models')

//show all areas
exports.index = async (req, res) => {

  //query the DB of all areas
  const configurations = await Configuration.findOne().exec() // 1 configuration only

  //send the response
  res.json({ data: configurations })

  return
}

// get all areas as string array
exports.getAreas = async(req, res) => {
  const configurations = await Configuration.find({}, 'name')
                .exec()

  res.json({data: configurations})

  return
}

//Store a new area
exports.store = async (req, res) => {
  //create a new area object with the request body
  const configuration = new Configuration(req.body)

  //save it in the DB
  await configuration.save()

  //send a 201 and the new resource
  res.status(201).json({ data: configuration })
}

//this function is for looking at one area by their mongo id
exports.show = async (req, res) => {

  //find this sneaky boye
  const configuration = await Configuration.findById(req.params.id).exec()

  //send him back home
  res.json({ data: configuration })
}

//ever wanted to make the areas disappear
exports.delete = async (req, res) => {

  //find the sneaky boye and make him go away
  await Configuration.findByIdAndRemove(req.params.id).exec()

  //let em know there aint no content no mo
  res.status(204).json()
}

//edit a area based on ID
exports.update = async (req, res) => {
  const area = await Configuration
  .findByIdAndUpdate(req.params.id, req.body, { new: true })
  .exec()
res.json({ data: area })
}
