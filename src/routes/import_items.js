//Here is where we declare the modules and shit we will need
const express = require('express')
const multer  = require('multer')
const fsCsv = require('fast-csv')
const mysql = require('mysql')
const chunk = require('chunk')

const upload = multer({ dest: 'public/files' })

//import the controllers and middleware
const { importItemsController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')
const { Item } = require('../database/models')

//set up the router
const router = express.Router()

router.post('/upload', upload.single('file'), catchErrors( importItemsController.upload ))

router.post('/get_db_items', catchErrors(importItemsController.getItemsFromDB))

router.post('/submit_upload', catchErrors(importItemsController.submitImportedItems))

module.exports = router
