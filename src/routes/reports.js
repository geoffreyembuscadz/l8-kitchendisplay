//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { reportsController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

/*
//get all users
router.get('/', catchErrors(reportsController.index))

//make a new boy
router.post('/', catchErrors(reportsController.store))

//see one boy
router.get('/:id', catchErrors(reportsController.show))

//update a boy
router.put('/:id', catchErrors(reportsController.update))

//get rid of a boy
router.delete('/:id', catchErrors(reportsController.delete))
*/
router.get('/trans_duration', catchErrors(reportsController.getTransactionsDuration))
router.get('/trans_details_duration/:transaction_id', catchErrors(reportsController.getTransactionDetailsDuration))



router.get('/export_trans_details_duration/:transaction_id', catchErrors(reportsController.exportTransactionDetailsDuration))

module.exports = router
