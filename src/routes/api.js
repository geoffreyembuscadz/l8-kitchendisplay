/*
 * This file is used to build the API routes, we may have
 * different routes for views and the like
 */

const express = require('express')

const userRoutes = require('./users') //use the user route shit

const areaRoutes = require('./areas')

const processRoutes = require('./processes')

const configurationRoutes = require('./configurations')

const itemRoutes = require('./items')

const toDoRoutes = require('./to_do')

const reportsRoutes = require('./reports')

const importItemsRoutes = require('./import_items')

const transactionsRoutes = require('./transactions')

const transactionDetailsRoutes = require('./transaction_details')

const transactionDetailsAuditRoutes = require('./transaction_details_audit')

const transactionDetailsTimerController = require('./transaction_details_timer')

const router = express.Router() //make a new router

router.use('/users', userRoutes) //tell it to use the userRoutes

router.use('/areas', areaRoutes) // tell it to use the areaRoutes

router.use('/configuration', configurationRoutes) // configuration load

router.use('/processes', processRoutes) // tell it to use the processRoutes

router.use('/items', itemRoutes) // tell it to use the itemRoutes

router.use('/to_do', toDoRoutes) // tell it to use the itemRoutes

router.use('/reports', reportsRoutes); // Reports

router.use('/import_items', importItemsRoutes)

router.use('/transactions', transactionsRoutes)

router.use('/transaction_details', transactionDetailsRoutes)

router.use('/transaction_details_audit', transactionDetailsAuditRoutes)

router.use('/transaction_details_timer', transactionDetailsTimerController)

module.exports = router
