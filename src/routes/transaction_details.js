//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { transactionDetailsController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all areas
router.get('/', catchErrors(transactionDetailsController.index))

//make a new boy
router.post('/', catchErrors(transactionDetailsController.store))

//see one boy
router.get('/:id', catchErrors(transactionDetailsController.show))

//update a boy
router.put('/:id', catchErrors(transactionDetailsController.update))

//get rid of a boy
router.delete('/:id', catchErrors(transactionDetailsController.delete))

module.exports = router
