//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { configurationController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all areas
router.get('/', catchErrors(configurationController.index))

//make a new boy
router.post('/', catchErrors(configurationController.store))

//see one boy
router.get('/:id', catchErrors(configurationController.show))

//update a boy
router.put('/:id', catchErrors(configurationController.update))

//get rid of a boy
router.delete('/:id', catchErrors(configurationController.delete))

module.exports = router
