//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { areasController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all areas
router.get('/', catchErrors(areasController.index))

//get all areas as string
router.get('/getAreas', catchErrors(areasController.getAreas))

//make a new boy
router.post('/', catchErrors(areasController.store))

//see one boy
router.get('/:id', catchErrors(areasController.show))

//update a boy
router.put('/:id', catchErrors(areasController.update))

//get rid of a boy
router.delete('/:id', catchErrors(areasController.delete))

module.exports = router
