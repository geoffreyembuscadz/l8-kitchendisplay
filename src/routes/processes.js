//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { processesController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all users
router.get('/', catchErrors(processesController.index))

//make a new boy
router.post('/', processesController.store)

//see one boy
router.get('/:id', catchErrors(processesController.show))

//update a boy
router.put('/:id', catchErrors(processesController.update))

//get rid of a boy
router.delete('/:id', catchErrors(processesController.delete))

module.exports = router
