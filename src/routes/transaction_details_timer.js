//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { transactionDetailsTimerController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all areas
router.get('/', catchErrors(transactionDetailsTimerController.index))

//make a new boy
router.post('/', catchErrors(transactionDetailsTimerController.store))

//see one boy
router.get('/:id', catchErrors(transactionDetailsTimerController.show))

//update a boy
router.put('/:id', catchErrors(transactionDetailsTimerController.update))

//get rid of a boy
router.delete('/:id', catchErrors(transactionDetailsTimerController.delete))

module.exports = router
