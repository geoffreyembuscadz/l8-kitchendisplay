//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { transactionsController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all areas
router.get('/', catchErrors(transactionsController.index))

//get all areas as string
router.get('/claim', catchErrors(transactionsController.getTransForClaim))

//make a new boy
router.post('/', catchErrors(transactionsController.store))

//see one boy
router.get('/:id', catchErrors(transactionsController.show))

//update a boy
router.put('/:id', catchErrors(transactionsController.update))

// Clear All Transactions with '1' status to '2'
router.post('/clear', catchErrors(transactionsController.clearClaims))


// [SECRET] Updater for Test
router.post('/undo_status', catchErrors(transactionsController.undoStatuses))

//get rid of a boy
router.delete('/:id', catchErrors(transactionsController.delete))

module.exports = router
