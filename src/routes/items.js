//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { itemsController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all users
router.get('/', catchErrors(itemsController.index))

//make a new boy
router.post('/', catchErrors(itemsController.store))

//see one boy
router.get('/:id', catchErrors(itemsController.show))

//update a boy
router.put('/:id', catchErrors(itemsController.update))

//get rid of a boy
router.delete('/:id', catchErrors(itemsController.delete))

module.exports = router
