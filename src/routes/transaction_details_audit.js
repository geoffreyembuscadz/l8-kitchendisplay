//Here is where we declare the modules and shit we will need
const express = require('express')

//import the controllers and middleware
const { transactionDetailsAuditController } = require('../controllers/index')
const { catchErrors } = require('../middleware/error-handler')

//set up the router
const router = express.Router()

//get all areas
router.get('/', catchErrors(transactionDetailsAuditController.index))

//make a new boy
router.post('/', catchErrors(transactionDetailsAuditController.store))

//see one boy
router.get('/:id', catchErrors(transactionDetailsAuditController.show))

//update a boy
router.put('/:id', catchErrors(transactionDetailsAuditController.update))

//get rid of a boy
router.delete('/:id', catchErrors(transactionDetailsAuditController.delete))

module.exports = router
