const express = require('express');
const http = require('http');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const socket_port = process.env.SOCKET_PORT || '8000'; // Socket Port
const port = process.env.PORT || '8080'; // API Port
const host = process.env.HOST || '127.0.0.1';
const exclude_drinks = process.env.EXCLUDE_DRINKS || 0;
const socket_url = 'http://' + host + ':' + socket_port + '/';

// const index = require('./src/routes/index');
const { apiRoutes } = require('./src/routes/index')
const { webROutes } = require('./src/routes/index')

// Models
const { Item, Transactions, TransactionDetails } = require('./src/database/models')

const app = express();


/***************************
* Socket server start
*/
const server = app.listen(socket_port, function() {
    console.log(`Socket Server running on port ${socket_port}`);
});

const io = require('socket.io')(server);

/**
* Socket IO connection
*/
io.on('connection', function(client) {
    console.log('Clients connected:');

    // room
    client.on('join', function(data) {
        console.log('Server: ', data);
    });

    /**
    * This will handle all the socket data from pos
    */
    client.on('pos_order', function(data) {
        console.log('POS data received.', data);
        // Save Transaction Data
        var transaction_doc_id = null;
        
        Transactions.count({
            'trans_id': data.trans_id,
            'table_name': data.table_name
        }, function (err, count) {
            var datetime = new Date();
            var current_datetime = datetime.toLocaleString(); /* Accurate Date & Time of Transaction's Created At */

            var trans_data = {
                _id: new mongoose.Types.ObjectId(),
                trans_id: data.trans_id,
                customer_name: data.customer_name,
                table_name: (data.table_name) ? data.table_name : '',
                service_type: data.service_type,
                priority_level: data.priority_level,
				discount_code: data.discount_code,
                created_at: current_datetime,
                completed_at: '',
                status: data.status,
                items: data.items
            };

            var items = [];

            if(count == 0){
                /* IF case for non-existing transaction, usually used in Settle */
                const transaction = new Transactions(trans_data)
                transaction.save()

                var new_trans_data = {
                    _id: trans_data._id,
                    trans_id: data.trans_id,
                    customer_name: data.customer_name,
                    table_name: (data.table_name) ? data.table_name : '',
                    service_type: data.service_type,
                    priority_level: data.priority_level,
					discount_code: data.discount_code,
                    created_at: current_datetime,
                    completed_at: '',
                    status: data.status,
                    items: []
                }

                Object.keys(data.items).forEach( itemKey => {
                    let serving_time = 0;
                    if(data.items[itemKey].serving_time)
                        serving_time = data.items[itemKey].serving_time;

                    var item_row_obj = {
                        _id: new mongoose.Types.ObjectId(),
                        id: data.items[itemKey].id,
                        trans_id: data.trans_id,
                        name: data.items[itemKey].name,
                        qty: data.items[itemKey].Qty,
                        department_id: data.items[itemKey].departmentId,
                        serving_time: serving_time,
                        current_step: 1,
                        note: data.items[itemKey].note,
                        running_status: 0,
                        status: 0,
                        current_area: ""
                    };

                    if(exclude_drinks == 1){
                        var exclude_drinks_item_name = data.items[itemKey].name;
                        exclude_drinks_item_name = exclude_drinks_item_name.replace(/\s/g, "");
                        exclude_drinks_item_name = exclude_drinks_item_name.toLowerCase();

                        if(!exclude_drinks_item_name.includes('12oz') || !exclude_drinks_item_name.includes('16oz') || !exclude_drinks_item_name.includes('water')){
                            items.push(item_row_obj);
                        }
                    } else {
                        items.push(item_row_obj);
                    }
                });

                // Save Transaction Item Data
                TransactionDetails.insertMany(items, function(error, docs){
                    console.log('docs of transdetails', docs);
                    // Tells the data is saved
                    console.log('POS data saved.', data);
                })
                new_trans_data.items = items;
                client.broadcast.emit('broad', new_trans_data);
            } else if(count > 0) {
                /* IF case for existing transaction, usually used in Save Trans */
                console.log('update trans', trans_data);
                Transactions.update({
                    'trans_id': data.trans_id,
                    'table_name': data.table_name
                }, trans_data, { multi: false }, (err, raw) => {
                    var trans_details_items = trans_data.items;
                    var trans_details_items_update = [];

                    Object.keys(trans_details_items).forEach(function(trans_details_keys){
                        if(trans_details_items[trans_details_keys].status !== undefined){
                            var new_trans_detail_items = {
                                id: trans_details_items[trans_details_keys].id,
                                trans_id: trans_data.trans_id,
                                name: trans_details_items[trans_details_keys].name,
                                qty: trans_details_items[trans_details_keys].Qty,
                                serving_time: 1,
                                note: trans_details_items[trans_details_keys].note,
                                status: 0
                            };
                            trans_details_items_update.push(new_trans_detail_items);
                        } else {
                            var update_trans_detail_items_where = {
                                id: trans_details_items[trans_details_keys].id,
                                trans_id: trans_data.trans_id
                            };
                            var update_trans_detail_items = {
                                qty: trans_details_items[trans_details_keys].Qty,
                                updatedAt: Date().toLocaleString()
                            };

                            TransactionDetails.update(update_trans_detail_items_where, update_trans_detail_items, { multi:false }, (err, raw) => { console.log ('existing quantity updated'); });
                        }
                    });

                    TransactionDetails.insertMany(trans_details_items_update, function(error, docs){
                        console.log('saving of adding order');
                        client.broadcast.emit('broad', trans_details_items_update);
                    });
                });
            }
        });
    });

    client.on('pos_void', function(data) {
        client.broadcast.emit('socket_void', data);
        console.log('socket_void: ', data);
    });

    client.on('communicate', function(data) {
        client.broadcast.emit('bumping', data);
        console.log('communicate: ', data.source);
    });

    client.on('claim', function(data) {
        /* Claiming Customer */
        client.broadcast.emit('claim', data);
        console.log('communicate: ', data.source);
    });

    client.on('claimed_trans', function(data) {
        console.log('appJS_claimed_trans_executed');

        client.broadcast.emit('transaction_claimed', data);
        console.log('claimed_trans: ', data.source);
    });
});
/**
* Socket server end
***************************/


app.use(cors())
app.set('port', port);

// Use native ES6 Promises since mongoose's are deprecated.
mongoose.Promise = global.Promise

// Connect to the database
mongoose.connect(process.env.MONGO_URI, { useMongoClient: true })

// Fail on connection error.
mongoose.connection.on('error', error => { throw error })


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', apiRoutes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
});

module.exports = app;


console.log(`API running on ${host}:${port}`);
